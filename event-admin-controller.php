<?php

namespace App\Http\Controllers\Admin\Events;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Events\Event;
use App\Models\Events\EventCategory;

class EventsController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.events.list')->with([
            'events'    => Event::all()->where('confirmed', 1)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.events.create')->with([
            'categories'    => EventCategory::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'title' => 'required|min:3',
            'poster' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $event = new Event;

        $event->title = $request->title;
        $event->slug = str_slug($request->title);

        if(!empty($request->image)) {
            $name = md5(time()) . '.' . $request->image->getClientOriginalExtension();
            $request->image->storeAs('events', $name, 'public');
            $event->image = $name;
        }

        $event->date = $request->date;
        $event->start_time = $request->start_time;
        $event->confirmed = 1;

        $event->save();

        $request->session()->flash('alert-success', 'New event added');
        return redirect()->route('events.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.events.delete')->with([
            'event'     => Event::findOrFail($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.events.edit')->with([
            'event'         => Event::findOrFail($id),
            'categories'    => EventCategory::all()

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|min:3',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        $event = Event::findOrFail($id);

        $event->title = $request->title;
        $event->slug = str_slug($request->title);


        if(!empty($request->image)) {
            $name = md5(time()) . '.' . $request->image->getClientOriginalExtension();
            $request->image->storeAs('events', $name, 'public');
            $event->image = $name;
        }

        $event->date = $request->date;
        $event->start_time = $request->start_time;
        $event->confirmed = 1;

        $event->save();

        $request->session()->flash('alert-warning', $request->title . ' updated successfully');
        return redirect()->route('events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        $event->delete();
        
        return redirect()->route('events.index')->with('alert-danger', $event->title . ' was deleted');
    }
}
