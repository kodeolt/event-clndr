<?php

namespace App\Http\Controllers\Admin\Events;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Events\Event;
use App\Models\Events\EventCategory;

class PageController extends Controller
{


   public function index() {

        return view('user.home.index')->with([
            'events' => Event::all()->where('confirmed', 1)
        ]);
    }

    public function event($slug) {
        
        $event = Event::where('slug', $slug)->first();

        // Cia events yra 'panasus'. Paimu 4 eventus su ta pacia kategorija, isskyrus ta pati rengini kuriame esu
        // event jau pats renginys su visa info

        return view('user.events.event')->with([
            'events' => Event::where('category_id', $event->category_id)->where('id', '!=', $event->id)->limit(4)->get(),
            'event' => $event
        ]);
    }


}
