<?php

namespace KlaipedaAsSuTavim\Models\Events;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{	


	// Jei neturi nei komentaru, nei kategoriju, galima palikti tiesiog tuscia

	// Eventas turi komentarus
    public function comments() {
    	return $this->hasMany('KlaipedaAsSuTavim\Models\Events\EventComment');
    }

    // Eventas turi kategorija
     public function category() {
    	return $this->belongsTo('KlaipedaAsSuTavim\Models\Events\EventCategory');
    }
}
